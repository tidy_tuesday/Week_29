---
title: "R Notebook"
output: html_notebook
---

```{r}
library(tidyverse)
library(wesanderson)

my_font <- "Comfortaa"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = "#f5f5f2", color = NA), 
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))
```

```{r}
all_ages <- read_csv("../data/all-ages.csv")
grad <- read_csv("../data/grad-students.csv")
recent_grad <- read_csv("../data/recent-grads.csv")
majors <- read_csv("../data/majors-list.csv")
```

```{r}
head(recent_grad)

```

```{r}

top10 <- recent_grad %>% filter(Sample_size > 30) %>% top_n(10, ShareWomen)
bottom10 <- recent_grad %>% filter(Sample_size > 30) %>% top_n(-10, ShareWomen)
top_bottom <- rbind(top10,bottom10)

top10 %>%
  mutate(Median_K = paste0(Median / 1000,"K")) %>%
  ggplot(aes(reorder(Major, ShareWomen), ShareWomen)) + 
  geom_col(fill = wes_palettes$Moonrise2[1]) +
  geom_text(aes(label = paste0(Major," ($",Median_K,")")), y=0.005, hjust = 0, vjust=0.1, col=wes_palettes$Moonrise2[4], family = my_font, size = rel(3.5)) + 
  geom_text(aes(label = paste0(round(ShareWomen*100,0),"%")), nudge_y = 0.03, col=wes_palettes$Moonrise2[1], family = my_font) +
  coord_flip() +
  my_theme +
  theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank()) +
  labs(title = "Top Majors of Recent Graduates, with Median Salaries",
       subtitle = "By Share of Women",
       x = "", y = "",
       caption = "Source: FiveThirtyEight")
  
ggsave("top_women.png")

```

```{r}

bottom10 %>%
  mutate(ShareMen = 1-ShareWomen,
         Median_K = paste0(Median / 1000,"K")) %>%
  ggplot(aes(reorder(Major, ShareMen), ShareMen)) + 
  geom_col(fill = wes_palettes$Moonrise2[2]) +
  geom_text(aes(label = paste0(Major," ($",Median_K,")")), y=0.005, hjust = 0, vjust=0.1, col=wes_palettes$Moonrise2[4], family = my_font, size = rel(3.5)) + 
  geom_text(aes(label = paste0(round(ShareMen*100,0),"%")), nudge_y = 0.03, col=wes_palettes$Moonrise2[2], family = my_font) +
  coord_flip() + my_theme +
  theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank()) +
  labs(title = "Top Majors of Recent Graduates, with Median Salaries",
       subtitle = "By Share of Men",
       x = "", y = "",
       caption = "Source: FiveThirtyEight")

ggsave("top_men.png")

```

