# Tidy Tuesday - Week 29
### College Majors And Income

I spent more time with the recent graduates data and visualized top majors by share of women, then the same by share of men. I revised it by adding median incomes for graduates with those majors after filtering out low sample sizes.

Observations:

1) There is a high percentage recent female graduates in education- and medical-related fields, which command median salaries of $28K to $48K.

2) There is a high percentage recent male graduates in engineering / tech fields (although Forestry stands out), and median salaries range from $35K to $60 (excluding $110K for Petroleum Engineering since it distorts the range).

3) I spend a disproportionate amount of time working on presention compared to properly analyzing the data. I should probably work on that. :)

See https://github.com/rfordatascience/tidytuesday for more information.

#### Top Majors of Recent Graduates - Women
![](top_women.png)

#### Top Majors of Recent Graduates - Men
![](top_men.png)